

from .architectures import *
from .gnn_base import GNN
from .gcn import GCN
from .gin import GIN
