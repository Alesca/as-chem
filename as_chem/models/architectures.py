
from .architectures_ import *

GNN_Architectures = {
    'gcn': gcn_layer,
    'gin': gin_layer,
    'gat': gat_layer
}
